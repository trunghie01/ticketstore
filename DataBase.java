import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import org.json.JSONArray;
import org.json.JSONObject;

public class DataBase {
    protected static String absolutePath = FileSystems.getDefault().getPath("./").toAbsolutePath().toString();

    static JSONObject getTable(String tableName){
        String tableString = readFile(tableName + ".txt");
        return new JSONObject( (tableString.length() <= 2) ? "{}" : tableString);
    }

    static int create(JSONObject object, String tableName){
        JSONObject table = getTable(tableName);
        int id = table.length()+1;
        while( table.has( String.valueOf(id) ) ) id++; // make id unique;
        table.put( String.valueOf(id) , object);
        writeFile( table.toString() , tableName+".txt");
        return id;
    }

    static String read(int id, String tableName){
        JSONObject table = getTable(tableName);
        return table.has( String.valueOf(id) ) ? table.getJSONObject( String.valueOf(id) ).toString() : "{}";
    }

    static void update(int id, JSONObject object, String tableName){
        JSONObject table = getTable(tableName);
        table.put( String.valueOf(id) , object);
        writeFile(table.toString(), tableName+".txt");
    }

    static void delete(int id, String tableName){
        JSONObject table = getTable(tableName);
        table.remove( String.valueOf(id)); // TODO remember to delete relationship
        writeFile(table.toString(), tableName+".txt");
    }

    static JSONArray where(String[][] conditions, String tableName){
        JSONArray answer = new JSONArray();
        JSONObject queryingTable = getTable(tableName);
        JSONArray ids = queryingTable.names();

        idLoop : for(Object id : ids){
            JSONObject queryingObject = queryingTable.getJSONObject(String.valueOf(id));
            for(String[] condition : conditions){
                if(condition.length != 3 || condition[0] == null || condition[1] == null || condition[2] == null) continue;

                String[] relationProp = condition[0].split("\\.");
                String prop = (relationProp.length > 1) ? relationProp[1] : relationProp[0];
                JSONObject relateTable = (relationProp.length > 1) ? getTable(relationProp[0] + "s") : queryingTable;
                JSONObject relateObject = (relationProp.length > 1) ?
                        relateTable.getJSONObject(queryingObject.get(relationProp[0] + "Id").toString()) : queryingObject;
                /* Example
                *  where({ {"price", "==", "1"}, {"plane.name", "==", 2} }, "ticket");
                *  => find objects with obj.price == 1 and obj.plane.name == 2
                *
                *  in the first for(condition : conditions) loop:
                *   relationProp = { "price" }
                *   prop = relationProp[0] = "price"
                *   relateTable = queryingTable = getTable("tickets")
                *   relateObject = queryingObject // a ticket
                *
                *  in the second loop:
                *   relationProp = { "plane", "name" }
                *   prop = relationProp[1] = "name"
                *   relateTable = getTable("planes")
                *   relateObject = relateTable.get(queryingObject.get("planeId")) // a plane
                */
                String comparator = condition[1];
                String value = condition[2];

                switch (comparator){
                    case "==":
                        if(!(relateObject.has(prop) && relateObject.get(prop).toString().equals(value))){
                            continue idLoop; // if exist 1 condition that not match then skip to the next object
                        }
                        break;
                    case "!=":
                        if(!(relateObject.has(prop) && !relateObject.get(prop).toString().equals(value))){
                            continue idLoop;
                        }
                        break;
                    case ">":
                        if(!(relateObject.has(prop) && Integer.parseInt(relateObject.get(prop).toString()) > Integer.parseInt(value))){
                            continue idLoop;
                        }
                        break;
                    case "<":
                        if(!(relateObject.has(prop) && Integer.parseInt(relateObject.get(prop).toString()) < Integer.parseInt(value))){
                            continue idLoop;
                        }
                        break;
                    case ">=":
                        if(!(relateObject.has(prop) && Integer.parseInt(relateObject.get(prop).toString()) >= Integer.parseInt(value))){
                            continue idLoop;
                        }
                        break;
                    case "<=":
                        if(!(relateObject.has(prop) && Integer.parseInt(relateObject.get(prop).toString()) <= Integer.parseInt(value))){
                            continue idLoop;
                        }
                        break;
                }
            }
            queryingObject.put("id", id);
            answer.put(queryingObject);
        }
        return answer;
    }

    static void seed(){
        int constNum = 19072001;
        File tickets = new File(DataBase.absolutePath, "tickets.txt");
        if (!tickets.isFile()){
            for(int i = 0; i < 10; i++){
                Ticket ticket = new Ticket(2001 + 100*i, constNum % (13+ 21*i) + "/07/2001", "Ha Noi", constNum % (13+ 21*i) + 3 + "/07/2001", "Bach Khoa");
                create(ticket.toJsonObject(), "tickets");
            }
        }
    }

    protected static void writeFile(String str, String fileName) {
        try {
            File file = new File(DataBase.absolutePath, fileName);
            if (!file.isFile()) file.createNewFile();
            
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(str.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected static String readFile(String fileName) {
        File file = new File(DataBase.absolutePath, fileName);
        byte[] bytes = new byte[((int) file.length())];
        String contents = "{}";
        try {
            if (!file.isFile()) file.createNewFile();
            FileInputStream fis = new FileInputStream(file);
            fis.read(bytes);
            contents = new String(bytes);
            fis.close();
            return contents;
        } catch (IOException e) {
            e.printStackTrace();
            writeFile("{}", fileName);
            return contents;
        }
    }
}
