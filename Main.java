import org.json.JSONArray;
import org.json.JSONObject;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.awt.*;
import java.util.function.Function;
import javax.swing.*;
class MyJButton extends JButton {
    MyJButton() {
        super();
        setHorizontalTextPosition(JButton.CENTER);
        setVerticalTextPosition(JButton.BOTTOM);
        setFont(new Font("Comic Sans",Font.BOLD,25));
        setForeground(Color.white);
        setBackground(new Color(0x70a1ff));
        setBorder(BorderFactory.createEtchedBorder());
        setLayout(null);
    }
}

public class Main {
    public static void main(String[] args){
        DataBase.seed();
        buildFrame();
    }

    static void buildFrame(){
        JButton ticketButton = new MyJButton();
        JButton planeButton = new MyJButton();
        JButton searchButton = new MyJButton();
        JButton staticButton = new MyJButton();
        JButton profitButton = new MyJButton();

        ticketButton.setText("Show Tickets");
        planeButton.setText("Show Planes");
        searchButton.setText("Search");
        staticButton.setText("Static");
        profitButton.setText("Profit");

        JPanel inputPanel = new JPanel();
        inputPanel.add(ticketButton);
        inputPanel.add(planeButton);
        inputPanel.add(searchButton);
        inputPanel.add(staticButton);
        inputPanel.add(profitButton);
        inputPanel.setBackground(new Color(0x3867d6));


        JPanel showBoard = new JPanel();
        showBoard.setLayout(new BoxLayout(showBoard, BoxLayout.Y_AXIS));

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS)); // make layout vertical
        panel.add(inputPanel);
        panel.add(showBoard);

        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(panel);

        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenu helpMenu = new JMenu("Help");

        JMenuItem exitItem = new JMenuItem("Exit");

        exitItem.addActionListener((e) -> System.exit(0));

        fileMenu.setMnemonic(KeyEvent.VK_F);// Alt + f for file
        helpMenu.setMnemonic(KeyEvent.VK_H); // Alt + h for help

        fileMenu.add(exitItem);

        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        JFrame frame = new JFrame("Tickets Store");
        frame.setSize(1000, 500);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // make program stop when close window
        frame.getContentPane().add(scroll);
        frame.setJMenuBar(menuBar);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        ImageIcon image = new ImageIcon("logo.jpg"); //create an ImageIcon
        frame.setIconImage(image.getImage()); //change icon of frame

        String[] ticketTitles = { "ID", "Price (000 VND)", "Plane ID", "Type", "Sold Time", "Actions"};
        String[] planeTitles = { "ID", "Land Place", "Land Time", "Fly Time" ,"Fly Place", "Actions"};
        ticketButton.addActionListener((e) -> showTable(ticketTitles, "tickets", showBoard, frame, Main::showTicketActions));
        planeButton.addActionListener((e) -> {
            JFrame newFrame = new JFrame();
            JPanel planePanel = new JPanel();
            planePanel.setLayout(new BoxLayout(planePanel, BoxLayout.Y_AXIS));
            JScrollPane scrollPane = new JScrollPane();
            scrollPane.setViewportView(planePanel);
            newFrame.setSize(1000, 500);
            newFrame.getContentPane().add(scrollPane);
            newFrame.setVisible(true);
            showTable(planeTitles, "planes", planePanel, newFrame, Main::showPlaneActions);
        });

        searchButton.addActionListener((e) -> searchButtonAction(showBoard, frame));
        staticButton.addActionListener((e) -> staticButtonAction(showBoard, frame));
        profitButton.addActionListener((e) -> profitButtonAction(showBoard, frame));
    }

    static void searchButtonAction(JPanel showBoard, JFrame frame){
        JTextField landPlaceField = new JTextField(5);
        JTextField landTimeField = new JTextField(5);
        JTextField flyTimeField = new JTextField(5);
        JTextField flyPlaceField = new JTextField(5);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("Land Place:"));
        myPanel.add(landPlaceField);
        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
        myPanel.add(new JLabel("Land Time:"));
        myPanel.add(landTimeField);
        myPanel.add(Box.createHorizontalStrut(15));
        myPanel.add(new JLabel("Fly Time:"));
        myPanel.add(flyTimeField);
        myPanel.add(Box.createHorizontalStrut(15));
        myPanel.add(new JLabel("Fly Place:"));
        myPanel.add(flyPlaceField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter Plane Values", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            String[][] conditions = new String[4][3];
            int index = 0;
            if(landPlaceField.getText().trim().length() > 0){
                conditions[index++] = new String[]{"plane.landPlace", "==", landPlaceField.getText()};
            }
            if(landTimeField.getText().trim().length() > 0){
                conditions[index++] = new String[]{"plane.landTime", "==", String.valueOf(parseDateInt(landTimeField.getText()))};
            }
            if(flyTimeField.getText().trim().length() > 0){
                conditions[index++] = new String[]{"plane.flyTime", "==", String.valueOf(parseDateInt(flyTimeField.getText()))};
            }
            if(flyPlaceField.getText().trim().length() > 0){
                conditions[index] = new String[]{"plane.flyPlace", "==", flyPlaceField.getText()};
            }

            JSONArray searchResult = DataBase.where(conditions, "tickets");
            String[] ticketTitles = { "ID", "Price  (000 VND)", "Plane ID", "Type", "Sold Time", };
            showSearchResult(ticketTitles, searchResult, showBoard, frame);
        }
    }

    static void staticButtonAction(JPanel showBoard, JFrame frame) {
        JTextField landTimeField = new JTextField(5);
        JTextField flyTimeField = new JTextField(5);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("Fly Time:"));
        myPanel.add(flyTimeField);
        myPanel.add(Box.createHorizontalStrut(15));
        myPanel.add(new JLabel("Land Time:"));
        myPanel.add(landTimeField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter Plane Values", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            JSONArray searchResult = searchTicketBetween(flyTimeField.getText()+"/07/2001", landTimeField.getText()+"/07/2001");
            String[] ticketTitles = { "ID", "Price (000 VND)", "Plane ID", "Type", "Sold Time", };
            showSearchResult(ticketTitles, searchResult, showBoard, frame);
        }
    }

    static void profitButtonAction(JPanel showBoard, JFrame frame) {
        JTextField fromField = new JTextField(5);
        JTextField toField = new JTextField(5);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("From:"));
        myPanel.add(fromField);
        myPanel.add(Box.createHorizontalStrut(15));
        myPanel.add(new JLabel("To:"));
        myPanel.add(toField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter Plane Values", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            JSONArray searchResult = searchTicketBetween(fromField.getText(), toField.getText());
            int profit = 0;
            for(var object : searchResult){
                JSONObject jObj = (JSONObject) object;
                int price = Integer.parseInt(jObj.get("price").toString());
                if(!jObj.get("soldTime").toString().equals("0")) profit += (Integer.parseInt(jObj.get("type").toString()) == 1) ? price * 5 / 100  : (price * 7 / 100 + 100);
            }
            JOptionPane.showMessageDialog(null,profit + " 000 VND");
        }
    }

    static JSONArray searchTicketBetween(String flyTime, String landTime){
        if(flyTime.length() != 10 || landTime.length() != 10) return new JSONArray();
        String[][] conditions = new String[2][3];
        int index = 0;
        if(flyTime.trim().length() > 0){
            conditions[index++] = new String[]{"plane.flyTime", ">=", String.valueOf(parseDateInt(flyTime))};
        }
        if(landTime.trim().length() > 0){
            conditions[index] = new String[]{"plane.landTime", "<=", String.valueOf(parseDateInt(landTime))};
        }

        return DataBase.where(conditions, "tickets");
    }

    static void refresh(JFrame frame){
        frame.invalidate();
        frame.validate();
        frame.repaint();
    }

    static void newTableData(String content,Object parent){
        JTextField td = new JTextField(10);
        td.setText(content);
        td.setEditable(false);
        ((JPanel) parent).add(td);
    }

    static void showTable(String[] titles, String tableName, JPanel showBoard, JFrame frame, Function<Integer, Void> showActions){
        var tickets  = DataBase.getTable(tableName);
        JPanel thead = new JPanel();
        for(var title : titles){
            newTableData(title, thead);
        }
        JPanel tbody = new JPanel();
        tbody.setLayout(new BoxLayout(tbody, BoxLayout.Y_AXIS));

        ArrayList<Integer> sortedIds = new ArrayList<>();
        for(var name : tickets.names()){
            sortedIds.add(Integer.parseInt(name.toString()));
        }
        Collections.sort(sortedIds);

        for(var name : sortedIds){
            var ticket = tickets.getJSONObject(name.toString());
            JPanel tr = new JPanel();
            newTableData(name.toString(), tr);
            for(var prop : ticket.names()){
                String propName = prop.toString();
                String content = ticket.get(propName).toString();
                if(propName.contains("Time")){
                    content = (content.length() != 8) ? "NaD" : parseDate(Integer.parseInt(content));
                }
                newTableData(content, tr);
            }
            JTextField actions = new JTextField(10);
            actions.setText("Add/Update/Delete");
            actions.setEditable(false);
            actions.setCursor(new Cursor(Cursor.HAND_CURSOR));
            actions.addMouseListener(new MouseAdapter(){
                @Override
                public void mouseClicked(MouseEvent e){
                    try {
                        showActions.apply(name);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            });
            tr.add(actions);
            tbody.add(tr);
        }
        showBoard.removeAll();
        showBoard.add(thead);
        showBoard.add(tbody);
        refresh(frame);
    }

    static Void showTicketActions(int id){
        JButton add = new JButton("Add Ticket");
        JButton update = new JButton("Update Ticket");
        JButton delete = new JButton("Delete Ticket");

        add.addActionListener((e) -> addNewTicket());
        update.addActionListener((e) -> updateTicket(id));
        delete.addActionListener((e) -> deleteTicket(id));

        JPanel myPanel = new JPanel();
        myPanel.add(add);
        myPanel.add(update);
        myPanel.add(delete);

        JOptionPane.showConfirmDialog(null, myPanel,"", JOptionPane.OK_CANCEL_OPTION);
        return null;
    }

    static Void showPlaneActions(int id){
        JButton add = new JButton("Add Plane");
        JButton update = new JButton("Update Plane");
        JButton delete = new JButton("Delete Plane");

        add.addActionListener((e) -> addNewPlane());
        update.addActionListener((e) -> updatePlane(id));
        delete.addActionListener((e) -> deletePlane(id));


        JPanel myPanel = new JPanel();
        myPanel.add(add);
        myPanel.add(update);
        myPanel.add(delete);

        JOptionPane.showConfirmDialog(null, myPanel,"", JOptionPane.OK_CANCEL_OPTION);
        return null;
    }

    static void showSearchResult(String[] titles, JSONArray Array, JPanel showBoard, JFrame frame){
        JPanel thead = new JPanel();
        for(var title : titles){
            newTableData(title, thead);
        }
        JPanel tbody = new JPanel();
        tbody.setLayout(new BoxLayout(tbody, BoxLayout.Y_AXIS));

        JSONArray sortedArray = new JSONArray();
        for(var object : Array){
            JSONObject jObj = (JSONObject) object;
            sortedArray.put(Integer.parseInt(jObj.get("id").toString()), jObj);
        }

        for(var object : sortedArray){
            if(object == JSONObject.NULL) continue;
            JSONObject jObj = (JSONObject) object;
            JPanel tr = new JPanel();
            newTableData(jObj.get("id").toString(), tr);
            for(var prop : jObj.names()){
                if(prop == "id") continue;
                String propName = prop.toString();
                String content = jObj.get(propName).toString();
                if(propName.contains("Time")){
                    content = (content.length() != 8) ? "NaD" : parseDate(Integer.parseInt(content));
                }
                newTableData(content, tr);
            }
            tbody.add(tr);
        }
        showBoard.removeAll();
        showBoard.add(thead);
        showBoard.add(tbody);
        refresh(frame);
    }

    static void addNewPlane(){
        JTextField flyPlaceField = new JTextField(5);
        JTextField flyTimeField = new JTextField(5);
        JTextField landPlaceField = new JTextField(5);
        JTextField landTimeField = new JTextField(5);


        JPanel myPanel = new JPanel();

        myPanel.add(new JLabel("Fly Place:"));
        myPanel.add(flyPlaceField);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("Fly Time:"));
        myPanel.add(flyTimeField);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("Land Place:"));
        myPanel.add(landPlaceField);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("Land Time:"));
        myPanel.add(landTimeField);
        myPanel.add(Box.createHorizontalStrut(15));


        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter New Plane Values", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) { // CREATE
            Plane plane = new Plane(flyTimeField.getText(), flyPlaceField.getText(),
                    landTimeField.getText(), landPlaceField.getText());
            DataBase.create(plane.toJsonObject(), "planes");
        }
    }

    static void deletePlane(int planeId){
        JPanel myPanel = new JPanel();

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Delete this Plane?", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) { // DELETE
            DataBase.delete(planeId, "planes");
        }
    }

    static void updatePlane(int planeId){
        JSONObject plane = new JSONObject(DataBase.read(planeId, "planes"));

        JTextField flyPlaceField = new JTextField(5);
        flyPlaceField.setText(plane.get("flyPlace").toString());
        JTextField flyTimeField = new JTextField(5);
        flyTimeField.setText(parseDate(plane.get("flyTime").toString()));
        JTextField landPlaceField = new JTextField(5);
        landPlaceField.setText(plane.get("landPlace").toString());
        JTextField landTimeField = new JTextField(5);
        landTimeField.setText(parseDate(plane.get("landTime").toString()));

        JPanel myPanel = new JPanel();

        myPanel.add(new JLabel("Fly Place:"));
        myPanel.add(flyPlaceField);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("Fly Time:"));
        myPanel.add(flyTimeField);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("Land Place:"));
        myPanel.add(landPlaceField);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("Land Time:"));
        myPanel.add(landTimeField);
        myPanel.add(Box.createHorizontalStrut(15));


        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter Plane Values", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) { // CREATE
            Plane planeObject = new Plane(flyTimeField.getText(), flyPlaceField.getText(),
                    landTimeField.getText(), landPlaceField.getText());
            DataBase.update(planeId, planeObject.toJsonObject(), "planes");
        }
    }

    static void addNewTicket(){
        JTextField priceField = new JTextField(5);
        JTextField planeIdField = new JTextField(5);

        String[] Type = {"Domestic", "International" };
        JComboBox typeIdField = new JComboBox(Type);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("Price:"));
        myPanel.add(priceField);
        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
        myPanel.add(new JLabel("Plane ID:"));
        myPanel.add(planeIdField);
        myPanel.add(Box.createHorizontalStrut(15));
        myPanel.add(new JLabel("Type:"));
        myPanel.add(typeIdField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter New Ticket Values", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) { // CREATE
            int type = (typeIdField.getSelectedItem() == "Domestic") ? 1 : 0;
            try {
                Ticket ticket = new Ticket(Integer.parseInt(priceField.getText()), Integer.parseInt(planeIdField.getText()), type);
                DataBase.create(ticket.toJsonObject(), "tickets");
            } catch(Exception e) {
                alert("Inputs are not valid!");
            }
        }
    }

    static void deleteTicket(int ticketId){
        JPanel myPanel = new JPanel();

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Delete this Ticket?", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) { // DELETE
            DataBase.delete(ticketId, "tickets");
        }
    }

    static void updateTicket(int ticketId){
        JSONObject ticket = new JSONObject(DataBase.read(ticketId, "tickets"));

        JTextField priceField = new JTextField(5);
        priceField.setText(ticket.get("price").toString());
        JTextField planeIdField = new JTextField(5);
        planeIdField.setText(ticket.get("planeId").toString());
        JTextField soldTimeField = new JTextField(5);
        soldTimeField.setText(parseDate(ticket.get("soldTime").toString()));

        String[] Type = {"Domestic", "International" };
        JComboBox typeIdField = new JComboBox(Type);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("Price:"));
        myPanel.add(priceField);
        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
        myPanel.add(new JLabel("Plane ID:"));
        myPanel.add(planeIdField);
        myPanel.add(Box.createHorizontalStrut(15));
        myPanel.add(new JLabel("Type:"));
        myPanel.add(typeIdField);
        myPanel.add(Box.createHorizontalStrut(15));
        myPanel.add(new JLabel("Sold Time:"));
        myPanel.add(soldTimeField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter Ticket Values", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try{
                int type = (typeIdField.getSelectedItem() == "Domestic") ? 1 : 0;
                Ticket ticketObject = new Ticket(Integer.parseInt(priceField.getText()), Integer.parseInt(planeIdField.getText()), type);
                ticketObject.setSoldTime(soldTimeField.getText());
                DataBase.update(ticketId, ticketObject.toJsonObject(), "tickets");
            } catch(Exception e) {
                alert("Inputs are not valid!");
            }
        }
    }

    static String parseDate(int date){
        if(date == 0) return "0";
        int day = date % 100;
        date = (date - day) / 100;
        int month = date % 100;
        date = (date - month) / 100;
        return ((day < 10) ? "0"+day : day) + "/" + ((month < 10) ? "0"+month : month) + "/" + date;
    }

    static String parseDate(String dateString){
        if(dateString.equals("0")) return "0";
        int date = Integer.parseInt(dateString);
        int day = date % 100;
        date = (date - day) / 100;
        int month = date % 100;
        date = (date - month) / 100;
        return ((day < 10) ? "0"+day : day) + "/" + ((month < 10) ? "0"+month : month) + "/" + date;
    }

    static int parseDateInt(String dateString){
        String[] dateArray = dateString.split("/");
        if(dateString.length() != 10 || dateArray.length != 3) return 0;
        return Integer.parseInt(dateArray[2]) * 10000 + Integer.parseInt(dateArray[1]) * 100 + Integer.parseInt(dateArray[0]);
    }

    static void alert(String content){
        JPanel myPanel = new JPanel();
        JTextField textField = new JTextField();
        textField.setText(content);
        textField.setEditable(false);
        myPanel.add(textField);
        JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter New Ticket Values", JOptionPane.OK_CANCEL_OPTION);
    }
}