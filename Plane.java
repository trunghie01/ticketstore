import org.json.JSONObject;

public class Plane {
    int flyTime;
    String flyPlace;
    int landTime;
    String landPlace;

    public Plane(String flyTime, String flyPlace, String landTime, String landPlace){
        this.setFlyTime(flyTime);
        this.flyPlace  = flyPlace;
        this.setLandTime(landTime);
        this.landPlace = landPlace;
    }

    public Plane(String jsonString){
        var json = new JSONObject(jsonString);
        this.flyTime     = json.getInt("flyTime");
        this.flyPlace    = json.getString("flyPlace");
        this.landTime    = json.getInt("landTime");
        this.landPlace   = json.getString("landPlace");
    }

    protected void setFlyTime(String flyTime){
        String[] dateArray = flyTime.split("/");
        if(flyTime.length() != 10 || dateArray.length != 3){
            this.flyTime = 0;
            return;
        }
        this.flyTime = Integer.parseInt(dateArray[2]) * 10000 + Integer.parseInt(dateArray[1]) * 100 + Integer.parseInt(dateArray[0]);
    }

    protected void setLandTime(String landTime){
        String[] dateArray = landTime.split("/");
        if(landTime.length() != 10 || dateArray.length != 3){
            this.landTime = 0;
            return;
        }
        this.landTime = Integer.parseInt(dateArray[2]) * 10000 + Integer.parseInt(dateArray[1]) * 100 + Integer.parseInt(dateArray[0]);
    }

    protected JSONObject toJsonObject(){
        var json = new JSONObject();
        json.put("flyTime", this.flyTime);
        json.put("flyPlace", this.flyPlace);
        json.put("landTime", this.landTime);
        json.put("landPlace", this.landPlace);
        return json;
    }
}
