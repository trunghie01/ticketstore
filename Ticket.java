import org.json.JSONObject;

public class Ticket {
    protected int type = 1; // 1 for homeland, 0 for global
    protected int price;
    protected int soldTime = 0; // TODO should change all date to string?
    protected int planeId;

    public Ticket(int price, int planeId, int type){
        this.price   = price;
        this.planeId = planeId;
        this.type = type;
    }

    public Ticket(int price, String flyTime, String flyPlace, String landTime, String landPlace){
        var plane = new Plane(flyTime, flyPlace, landTime, landPlace);
        var planeId = DataBase.create(plane.toJsonObject(), "planes"); // TODO check if plane exists

        this.price   = price;
        this.planeId = planeId;
    }

    public Ticket(String jsonString){
        var json = new JSONObject(jsonString);
        this.type     = json.getInt("type");
        this.price    = json.getInt("price");
        this.soldTime = json.getInt("soldTime");
        this.planeId  = json.getInt("planeId");
    }

    protected JSONObject toJsonObject(){
        var json = new JSONObject();
        json.put("type", this.type);
        json.put("price", this.price);
        json.put("soldTime", this.soldTime);
        json.put("planeId", this.planeId);
        return json;
    }

    protected void makeGlobal(){
        this.type = 0;
    }

    protected boolean isSold(){
        return this.soldTime != 0;
    }

    protected void setSoldTime(String soldTime) {
        String[] dateArray = soldTime.split("/");
        if(soldTime.length() != 10 || dateArray.length != 3){
            this.soldTime = 0;
            return;
        }
        this.soldTime = Integer.parseInt(dateArray[2]) * 10000 + Integer.parseInt(dateArray[1]) * 100 + Integer.parseInt(dateArray[0]);
    }

    protected Plane getPlane(){
        var jsonString = DataBase.read(this.planeId, "planes");
        return (jsonString.length() >= 2) ? new Plane(jsonString) : null;
    }
}